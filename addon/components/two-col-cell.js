import Component from '@ember/component';
import { computed } from '@ember/object';
import md5 from 'md5';

export default Component.extend({
  tagName: 'div',
  attributeBindings: null,
  classNameBindings: null,
  classNames: null,
  modelName: '',
  fieldName: '',
  disabled: false,
  disabledCheckAll: false,
  dbid: 0,
  init() {
    this._super(...arguments);
    this.set('attributeBindings', ['name']);
    this.set('classNameBindings', ['selected', 'rowChecked']);
    this.set('classNames', ['two-col-select-row']);
  },
  name: computed('formName', 'fieldName', 'dbid', function () {
    return 'TwoColSelect_' + this.get('formName') + '_' + this.get('fieldName') + '_' + md5(this.get('formName') + '_' + this.get('fieldName') + '_' + this.get('dbid'));
  }),
  selected: computed('mouseDragData.selectedRows.@each', 'item.index', 'item.checked', function(){
    return this.get('mouseDragData.selectedRows').indexOf(this.get('item.index')) !== -1;
  }),
  rowChecked: computed('mouseDragData.selectedRows.@each', 'item.index', 'item.checked', function(){
    return this.get('item.checked');
  })
});
