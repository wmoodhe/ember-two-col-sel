import { computed } from '@ember/object';
import Component from '@ember/component';
import { A } from '@ember/array';
import { inject as service } from '@ember/service';
import TwoColSelectData from '../utils/two-col-select-data';
import TwoColSelectFilters from '../utils/two-col-select-filters';
import { runTask, runDisposables } from 'ember-lifeline';
import moment from 'moment';
import MultisearchFilterMixin from 'ember-multisearch-sortable-tables/mixins/multisearch-filter-mixin';

export default Component.extend(MultisearchFilterMixin, {

  init() {
    this._super(...arguments);

    if (this.get("filterList")){
      this.setSearchFilters(this.get("filterList"));
    }

    let rightColData = TwoColSelectData.create();
    // ensures not objects that are destoryed somehow to into list
    this.set('rightTableDataList', this.get('rightTableDataList').filterBy('isDestroyed', false));
    rightColData.loadArray(this.get('rightTableDataList'));
    this.set('rightTableData', rightColData);
    this.get('rightTableData').getList().forEach(function(item){
      item.set('checked', false);
    })

    let leftColData = TwoColSelectData.create();
    // ensures not objects that are destroyed somehow to into list
    if (this.get('leftTableDataList')) {
      this.set('leftTableDataList', this.get('leftTableDataList').filterBy('isDestroyed', false));
      leftColData.loadArray(this.get('leftTableDataList'));
    }

    this.set('leftTableData', leftColData);
    this.get('leftTableData').getList().forEach(function(item){
      item.set('checked', false);
    });
    //right col data needs to be set in init() of component since this can take differnt forms.


    $('.right-col-header').height($('.left-col-header').height());

    this.set('leftMouseDragData',{
      'selectedRows': A(),
      'selectedIds': A(),
      'dragging': false
    });

    this.set('rightMouseDragData',{
      'selectedRows': A(),
      'selectedIds': A(),
      'dragging': false
    });

    },

  didRender(){
    this._super(...arguments);
    let twoColSelectId = this.getWithDefault('twoColSelectId', 'twoColSelect');
    $('#'+twoColSelectId).find('.right-col-header').height($('#'+twoColSelectId).find('.left-col-header').height());
    runTask(this, () => {
      $('#'+twoColSelectId).find('.right-col-header').height($('#'+twoColSelectId).find('.left-col-header').height());
    }, 100);
  },

  willDestroy() {
    this._super(...arguments);

    runDisposables(this);
  },

  /* Defaults */
  store: service('store'),
  removeFromLeft: true, // Allow items to be removed from the left side
  removeFromRight: true, // Allow items to eb removed from the right side
  grouping: false, // Group items by a field
  groupByColumn: '', // Which field to group items by, if grouping is enabled
  rowLabel: "name",
  showFilters: false,
  sortKey: "name",
  customData: null,
  somethingMoved: false,
  hasFilter: true,
  leftAllChecked: false,
  rightAllChecked: false,
  leftMouseDragData: null,
  rightMouseDragData: null,
  maxAssignable: null,
  filterObjectsTarget: 'filteredItems',
  searchFilters: null,
  user: null,
  columnAttribute: null,
  columnAttributeHeader: null,
  labelHeader: null,
  

  allTableDataList: computed('leftTableDataList.@each', 'rightTableDataList.@each', function(){
    return this.get("leftTableDataList").concat(this.get("rightTableDataList"));
  }),

  rightGrouping: computed('grouping', 'rightGrouped', function(){
    return this.get('grouping') || this.get('rightGrouped');
  }),
    filteredLeftTableData: computed( 'filteredItems.@each','leftTableData.list.length', 'leftTableData',
   'leftTableFilter.filters.@each.searchValue',function(){

    let filteredResults = this. filterColumn(this.get('leftTableData').getList(),this.get('filteredItems'));
    return filteredResults;
  }),
  filteredRightTableData: computed('filteredItems.@each','rightTableData.list.length', 'rightTableData',
   'rightTableFilter.filters.@each.searchValue', function(){
     let filteredResults = this.filterColumn(this.get('rightTableData').getList(),this.get('filteredItems'));
     return filteredResults;
  }),

  currentLeftColResults: computed('filteredLeftTableData.length', function(){
    return this.get('filteredLeftTableData').length;
  }),
  totalLeftColResults: computed('leftTableData.list.length', function(){
    return this.get('leftTableData').getList().length;
  }),
  currentRightColResults: computed('filteredRightTableData.length', function(){
    return this.get('filteredRightTableData').toArray().length;
  }),
  totalRightColResults: computed('rightTableData.list.length', function(){
    return this.get('rightTableData').getList().toArray().length;
  }),
  leftSideEmpty: computed('currentLeftColResults', function(){
    return this.get('currentLeftColResults') === 0;
  }),
  rightSideEmpty: computed('totalRightColResults', function(){
    return this.get('currentRightColResults') === 0;
  }),

  currentNumberToAssign: computed('maxAssignable', 'totalRightColResults', 'filteredLeftTableData.@each.checked', function() {
    if(this.get('maxAssignable')){
      return this.get('totalRightColResults') + this.get('leftTableData').getList().filterBy('checked', true).length;
    }
    return null;
  }),

  currentNumberAvailableToAssign: computed('maxAssignable', 'currentNumberToAssign',function() {
    return this.get('maxAssignable') - this.get('currentNumberToAssign');
  }),

  canAssignMore: computed('maxAssignable', 'currentNumberToAssign', 'currentNumberAvailableToAssign', function() {
    if(this.get('maxAssignable')){
      if(this.get('currentNumberAvailableToAssign') <= 0){
        var options = {
          closeButton: false,
          preventDuplicates: true,
          timeOut: 0,
          tapToDismiss: false,
          onclick: false,
          closeOnHover: false
        }
        if(this.get('currentNumberAvailableToAssign') < 0) {
          this.toast.error(this.get('maxAssignableMsg'), 'Warning', options);
        }
        return false;
      }
    }
    return true;
  }),

  hideCheckAllLeft: computed('canAssignMore', function() {
    if(this.get('maxAssignable')){
      return this.get('maxAssignable') - this.get('leftTableData').getList().length < 0;
    }
    return false;
  }),

  currentLeftColResults: computed('filteredLeftTableData.length', function(){
    return this.get('filteredLeftTableData').toArray().length;
  }),
  totalLeftColResults: computed('leftTableData.list.length', function(){
    return this.get('leftTableData').getList().toArray().length;
  }),

  moveItems: function(ids, sourceSide, targetSide){
    this.sendAction('moveItems', ids, sourceSide, targetSide);
  },

  filterColumn(data,filteredItems){

    if(typeof filteredItems !== "undefined"){
        data = data.filter(function(item){
        return filteredItems.includes(item);
      });
    }

    if(this.get('grouping')){
      data.sortBy(this.get('groupByColumn'), this.get('sortKey')).forEach(function(item, index) {
        item.set('index', index);
      });
    }else{
      data.sortBy(this.get('sortKey')).forEach(function(item, index) {
        item.set('index', index);
      });
    }

    return data;
  },

  actions: {
      move(){
        var leftItems = this.get('leftTableData').getList().filterBy('checked', true);
        var rightItems = this.get('rightTableData').getList().filterBy('checked', true);
        //move from left to right
        this.send("moveItems", leftItems, "left", "right");
        this.moveItems(leftItems,"left", "right");

        //move from right to left
        this.send("moveItems", rightItems, "right", "left");
        this.moveItems(rightItems, "right", "left");

        this.setProperties({
          leftAllChecked: false,
          rightAllChecked: false
        });
      },
      moveItems: function(itemsToMove, sourceSide){
        var source, dest;
        if(sourceSide === "left"){
          source = this.get('leftTableData');
          dest = this.get('rightTableData');
        }else{
          source = this.get('rightTableData');
          dest = this.get('leftTableData');
        }

        if (this.get("moveEachItem")) {
          this.moveEachItem(itemsToMove, source, dest, sourceSide);
        }
        else {
          itemsToMove.forEach(function(item) {
            dest.addObject(item);
            source.removeObject(item);
            item.set('checked', false);
          }.bind(this));
        }
      }
  }

});
