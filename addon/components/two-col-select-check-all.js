import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  checkBoxVal: false,

  isEmpty: computed('tableData.[]', function() {
    return !this.get('tableData.length');
  }),

  actions: {
    checkAll: function(tableData) {
      const checked = !this.get('checkBoxVal');
      this.set('checkBoxVal', checked);

      tableData.forEach(item => {
        if (!item.get('disabled') && !item.get('disabledCheckAll')) {
          item.set('checked', checked);
        }
      });
    }
  }
});
